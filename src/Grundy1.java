import java.util.*;
class Grundy1 {
    void principal() {
        // Appel de la méthode "partieJoueurContreJoueur" pour lancer une partie
        partieJoueurContreJoueur();
    }
    
    /**
    * creer un tableau avec des allumettes 
    * @param nb nombre d'allumettes
    * @return un tableau avec le nombre d'allumettes
    **/
    char[] creerTableauAllumettes(int nb) {
        char[] ret = new char[nb];
        int i = 0;
        while (i < ret.length) {
            ret[i] = '|';
            i = i + 1;
        }
        return ret;
    }
  
    /**
    * teste de creerTableauAllumettes
    **/
    void testCreerTableauAllumettes() {
        System.out.println();
		System.out.println("*** testCreerTableauAllumettes()");
        
        char[] a = {'|', '|'};
        testCasCreerTableauAllumettes(2, a);
       
        char[] b = {};
        testCasCreerTableauAllumettes(0, b);
       
        char[] c = {'|'};
        testCasCreerTableauAllumettes(1, c);
    }
   
    /**
    * teste un appel de creerTableauAllumettes
    * @param lg longueur du tableau
    * @param result resultat attendu
    **/
    void testCasCreerTableauAllumettes(int lg, char[] result) {
        // Arrange
        System.out.print("creerTableauAllumettes (" + lg + ") \t= " + Arrays.toString(result) + "\t : ");
        // Act
        char[] resExec = creerTableauAllumettes(lg);
        boolean pareil = Arrays.equals(resExec, result);
        // Assert
        if (pareil){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * affiche un tableau d'allumettes
    * @param tab tableau d'allumettes
    */
    void afficherTableauAllumettes(char[] tab) {
        for (int i = 0 ; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }
    
    /**
	* Test de affichertableauAllumettes()
	*/
	void testAfficherTableauAllumettes() {
		System.out.println();
		System.out.println("*** testAfficherTableauAllumettes()");

		char[] a = {};
		testCasAfficherTableauAllumetttes(a);
	
		char[] b = {'|'};
		testCasAfficherTableauAllumetttes(b);
        
        char[] c = {'|', '|'};
		testCasAfficherTableauAllumetttes(c);
        
        char[] d = {'|', '|', '|', '|'};
		testCasAfficherTableauAllumetttes(d);
	}

	/**
	* teste un appel de afficherTableauAllumettes()
	* @param tab tableau de charactères
	*/
	void testCasAfficherTableauAllumetttes(char[] tab) {
		System.out.print("afficherTableauAllumettes (" + Arrays.toString(tab) + ") : ");
		afficherTableauAllumettes(tab);
	}
    
    /**
    * affiche les différents tableaux d'allumettes
    * @param tab tableau d'entiers
    **/
    void creerEtAfficherLesTableaux(int[] tab) {
        int i = 0;
        int ligne = 0;
        while (i < tab.length) {
            if (tab[i] >= 1) {
                char[] creerTab = creerTableauAllumettes(tab[i]);
                System.out.print(ligne + " : ");
                afficherTableauAllumettes(creerTab);
                ligne = ligne + 1;
            }
            i = i + 1;
        }
    }
    
    /**
    * teste de creerEtAfficherLesTableaux
    **/
    void testCreerEtAfficherLesTableaux() {
        System.out.println();
		System.out.println("*** testCreerEtAfficherLesTableaux()");
        
        int[] a = {11, 3, 4, 2, 1, 1, 0, 0, 0, 0, 0};
        System.out.println("creerEtAfficherLesTableaux (" + Arrays.toString(a) + ")");
        creerEtAfficherLesTableaux(a);
        
        System.out.println();
        
        int[] b = {7, 3, 5, 1, 0, 0, 4, 0, 0, 0, 0};
        System.out.println("creerEtAfficherLesTableaux (" + Arrays.toString(b) + ")");
        creerEtAfficherLesTableaux(b);
    }
    
    /**
    * teste si le joueur donne un bon numéro de ligne
    * @param tab tableau d'entiers
    * @param ligne la ligne que le joueur séléctionne
    * @return vrai ssi la ligne est bonne
    **/
    boolean bonChoixLigne(int[] tab, int ligne) {
        boolean ret = true;
        if (ligne < 0 || ligne >= tab.length) {
            ret = false;
        } else {
            if (tab[ligne] <= 2) {
                ret = false;
            }
        }
        return ret;
    }
   
    /**
    * teste de bonChoixLigne
    **/
    void testBonChoixLigne() {
        System.out.println();
		System.out.println("*** testBonChoixLigne()");
        
        int[] a = {2, 3, 5, 1, 0, 0, 0, 0, 0, 0, 0};
        testCasBonChoixLigne(a, -1, false);
        testCasBonChoixLigne(a, 0, false);
        testCasBonChoixLigne(a, 11, false);
        testCasBonChoixLigne(a, 12, false);
        testCasBonChoixLigne(a, 3, false);
        testCasBonChoixLigne(a, 1, true);
        testCasBonChoixLigne(a, 2, true);
    }
   
    /**
    * teste un appel de bonChoixLigne
    * @param tab tableau d'entiers
    * @param ligne la ligne que le joueur séléctionne
    * @param result resultat attendu
    **/
    void testCasBonChoixLigne(int[] tab, int ligne, boolean result) {
        // Arrange
        System.out.print("BonChoixLigne (" + Arrays.toString(tab) + ", " + ligne + ") \t= " + result + "\t : ");
        // Act
        boolean resExec = bonChoixLigne(tab, ligne);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
   
    /**
    * teste si le tableau peut etre séparé par le nombre qu'a rentré le joueur
    * @param tab tableau d'entiers
    * @param ligne la ligne que le joueur selectionne
    * @param separer le nombre d'allumettes que je joueur veux séparer
    * @return vrai ssi c'est separable
    **/
    boolean bonneSeparation(int[] tab, int ligne, int separer) {
        boolean ret = false;
        if (bonChoixLigne(tab, ligne)) {
            if (tab[ligne] % 2 == 0) {
                if ((separer > 0) && (separer < tab[ligne]) && (separer != (tab[ligne] / 2))) {
                    ret = true;
                }
            } else {
                if ((separer > 0) && (separer < tab[ligne])) {
                    ret = true;
                }
            }
        }
        return ret;
    }
    
    /**
    * teste de bonneSeparation
    **/
    void testBonneSeparation() {
        System.out.println();
		System.out.println("*** testBonneSeparation()");
        
        int[] a = {2, 3, 5, 1, 4, 0, 0, 0, 0, 0, 0};
        testCasBonneSeparation(a, 1, 1, true);
        testCasBonneSeparation(a, 1, 2, true);
        testCasBonneSeparation(a, 1, 3, false);
        
        testCasBonneSeparation(a, 4, 2, false);
        testCasBonneSeparation(a, 4, -1, false);
        testCasBonneSeparation(a, 4, 4, false);
        testCasBonneSeparation(a, 4, 1, true);
        
        testCasBonneSeparation(a, 0, 2, false);
        testCasBonneSeparation(a, 0, 1, false);
        
        testCasBonneSeparation(a, 6, 2, false);
    }
   
    /**
    * teste un appel de bonneSeparation
    * @param tab tableau d'entiers
    * @param ligne la ligne que le joueur séléctionne
    * @param separer le nombre d'allumettes que le joueur veux séparer
    * @param result resultat attendu
    **/
    void testCasBonneSeparation(int[] tab, int ligne, int separer, boolean result) {
        // Arrange
        System.out.print("bonneSeparation (" + Arrays.toString(tab) + ", " + ligne + ", " + separer + ") \t= " + result + "\t : ");
        // Act
        boolean resExec = bonneSeparation(tab, ligne, separer);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }

    /**
    * renvoie l'indice du premier zero d'un tableau (le tableau comporte forcémment un zero)
    * @param tab tableau d'entiers 
    **/
    int premierZero(int[] tab) {
        int ret = -1;
        boolean sortir = false;
        while ((ret < tab.length) && !sortir) {
            ret = ret + 1;
            if (tab[ret] == 0) {
                sortir = true;
            }
        }
        return ret;
    }
    
    /**
    * teste de premierZero
    **/
    void testPremierZero() {
        System.out.println();
		System.out.println("*** testPremierZero()");
        
        int[] a = {9, 3, 6, 1, 2, 0, 0, 0, 0};
        testCasPremierZero(a, 5);
        
        int[] b = {8, 3, 5, 0, 0, 0, 0, 0};
        testCasPremierZero(b, 3);
        
        int[] c = {0};
        testCasPremierZero(c, 0);
        
        int[] d = {8, 3, 0, 1, 2, 0, 0, 0};
        testCasPremierZero(d, 2);
    }
   
    /**
    * teste un appel de premierZero
    * @param tab tableau d'entier
    * @param result resultat attendu
    **/
    void testCasPremierZero(int[] tab, int result) {
        // Arrange
        System.out.print("premierZero (" + Arrays.toString(tab) + ") \t= " + result + "\t : ");
        // Act
        int resExec = premierZero(tab);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }

    /**
    * separe une ligne d'un tableau
    * @param tab tableau d'entiers
    * @param ligne la ligne que le joueur séléctionne
    * @param separer le nombre d'allumettes que le joueur veut séparer
    * @return tableau avec la séparation faite
    **/
    int[] separation(int[] tab, int ligne, int separer) {
        int tmp;
        if (bonneSeparation(tab, ligne, separer)) {
            if (tab[ligne + 1] == 0) {
                tmp = tab[ligne] - separer;
                tab[ligne + 1] = tmp;
                tab[ligne] = separer;
            } else {
                int zero = premierZero(tab);
                tmp = tab[ligne] - separer;
                tab[zero] = tmp;
                tab[ligne] = separer;
            }
        }
        return tab;
    }
    
    /**
    * teste de separation
    **/
    void testSeparation() {
        System.out.println();
		System.out.println("*** testSeparation()");
        
        int[] a = {3, 5, 4, 3, 0, 0, 0, 0, 0};
        int[] resultA = {3, 2, 4, 3, 3, 0, 0, 0, 0};
        testCasSeparation(a, 1, 2, resultA);
        
        int[] b = {3, 5, 4, 3, 0, 0, 0, 0, 0};
        int[] resultB = {3, 5, 4, 2, 1, 0, 0, 0, 0};
        testCasSeparation(b, 3, 2, resultB);
        
        int[] c = {3, 5, 4, 3, 0, 0, 0, 0, 0};
        int[] resultC = {1, 5, 4, 3, 2, 0, 0, 0, 0};
        testCasSeparation(c, 0, 1, resultC);
    }
   
    /**
    * teste un appel de separation
    * @param tab tableau d'entier
    * @param ligne la ligne que le joueur séléctionne
    * @param separer le nombre d'allumettes que le joueur veut séparer
    * @param result resultat attendu
    **/
    void testCasSeparation(int[] tab, int ligne, int separer, int[] result) {
        // Arrange
        System.out.print("separation (" + Arrays.toString(tab) + ", " + ligne + ", " + separer + ") \t= " + Arrays.toString(result) + "\t : ");
        // Act
        int[] resExec = separation(tab, ligne, separer);
        // Assert
        if (Arrays.equals(resExec, result)){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * determine qui va jouer
    * @param joueur numéro qui est soit paire ou impaire
    * @param joueur1 nom du premier joueur
    * @param joueur2 nom du deuxième joueur 
    * @return renvoie le nom du joueur1 si le numéro est paire et inversement
    **/
    String quiVaJouer(int joueur, String joueur1, String joueur2) {
        String ret;
        if (joueur % 2 == 0) {
            ret = joueur1;
        } else {
            ret = joueur2;
        }
        return ret;
    }
    
    /**
    * teste de quiVaJouer
    **/
    void testQuiVaJouer() {
        System.out.println();
		System.out.println("*** testQuiVaJouer()");
        
        String j1 = "nathan";
        String j2 = "basol";
        testCasQuiVaJouer(1, j1, j2, "basol");
        testCasQuiVaJouer(0, j1, j2, "nathan");
        testCasQuiVaJouer(4, j1, j2, "nathan");
        testCasQuiVaJouer(5, j1, j2, "basol");
    }
   
    /**
    * teste un appel de quiVaJouer
    * @param joueur numéro qui est soit paire ou impaire
    * @param joueur1 nom du premier joueur
    * @param joueur2 nom du deuxième joueur 
    * @param result resultat attendu
    **/
    void testCasQuiVaJouer(int joueur, String joueur1, String joueur2, String result) {
        // Arrange
        System.out.print("quiVaJouer (" + joueur + ", " + joueur1 + ", " + joueur2 + ") \t= " + result + "\t : ");
        // Act
        String resExec = quiVaJouer(joueur, joueur1, joueur2);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * demande au joueur le nombre d'allumettes de départ tant que le nombre d'allumettes saisis est incorrecte
    * @return un tableau d'entier avec l'indice 0 initialisé 
    **/
    int[] demandeNombreAllumettes() {
        int initialisation = SimpleInput.getInt("Nombre d'allumettes : "); 
        while (initialisation <= 2) {
            initialisation = SimpleInput.getInt("Nombre d'allumettes : "); 
        }
        int[] ret = new int[initialisation];
        ret[0] = initialisation;
        return ret;
    }
    
    /**
    * teste de demandeNombreAllumettes
    **/
    void testDemandeNombreAllumettes() {
        System.out.println();
		System.out.println("*** testDemandeNombreAllumettes()");
        
        System.out.println("rentrez 1 puis 2 puis 7 : ");
        System.out.println("le programme renvoit ok si la méthode marche bien");
        int[] a = {7, 0, 0, 0, 0, 0, 0};
        testCasDemandeNombreAllumettes(a);
        
        System.out.println();
        
        System.out.println("rentrez 13 : ");
        System.out.println("le programme renvoit ok si la méthode marche bien");
        int[] b = {13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        testCasDemandeNombreAllumettes(b);
    }
   
    /**
    * teste un appel de demandeNombreAllumettes
    * @param result resultat attendu
    **/
    void testCasDemandeNombreAllumettes(int[] result) {
        // Arrange
        System.out.println("demandeNombreAllumettes (" + " nombres rentrés par le testeur " + ") \t= " + Arrays.toString(result) + "\t ");
        // Act
        int[] resExec = demandeNombreAllumettes();
        // Assert
        if (Arrays.equals(resExec, result)){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * vérifie si un sul tas est séparable
    * @param tab tableau d'entiers
    * @return vrai ssi un seul tas est séparabme 
    **/
    boolean unSeulTasSeparable(int[] tab) {
        boolean ret;
        int compteur = 0;
        int i = 0;
        while (i < tab.length) {
            if (tab[i] > 2) {
                compteur = compteur + 1;
            }
            i = i + 1;
        }
        if (compteur == 1) {
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }
    
    /**
    * teste de unSeulTasSeparable
    **/
    void testUnSeulTasSeparable() {
        System.out.println();
		System.out.println("*** testUnSeulTasSeparable()");
        
        int[] a = {2, 1, 3, 2};
        testCasUnSeulTasSeparable(a, true);
        int[] b = {2, 4, 3, 2};
        testCasUnSeulTasSeparable(b, false);
        int[] c = {3};
        testCasUnSeulTasSeparable(c, true);
    }
   
    /**
    * teste un appel de unSeulTasSeparable
    * @param tab tableau d'entier
    * @param result resultat attendu
    **/
    void testCasUnSeulTasSeparable(int[] tab, boolean result) {
        // Arrange
        System.out.print("unSeulTasSeparable (" + Arrays.toString(tab) + ") \t= " + result + "\t : ");
        // Act
        boolean resExec = unSeulTasSeparable(tab);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * cherche l'indice du seul tas séparable
    * @param tab tableau d'entiers avec un seul tas séparable 
    * @return l'indice du seul tas séparable
    **/
    int indiceDuSeulTasSeparable(int[] tab){
        int ret = 0;
        int i = 0;
        boolean sort = false;
        while ((i < tab.length) && !sort) {
            if (tab[i] > 2) {
                ret = i;
                sort = true;
            }
            i = i + 1;
        }
        return ret;
    }
    
    /**
    * teste de indiceDuSeulTasSeparable
    **/
    void testIndiceDuSeulTasSeparable() {
        System.out.println();
		System.out.println("*** testIndiceDuSeulTasSeparable()");
      
        int[] a = {3, 1, 2, 1};
        testCasIndiceDuSeulTasSeparable(a, 0);
        int[] b = {1, 1, 2, 3};
        testCasIndiceDuSeulTasSeparable(b, 3);
        int[] c = {1, 3, 2, 1};
        testCasIndiceDuSeulTasSeparable(c, 1);
        int[] d = {3};
        testCasIndiceDuSeulTasSeparable(d, 0);
    }
   
    /**
    * teste un appel de indiceDuSeulTasSeparable
    * @param tab tableau d'entier
    * @param result resultat attendu
    **/
    void testCasIndiceDuSeulTasSeparable(int[] tab, int result) {
        // Arrange
        System.out.print("indiceDuSeulTasSeparable (" + Arrays.toString(tab) + ") \t= " + result + "\t : ");
        // Act
        int resExec = indiceDuSeulTasSeparable(tab);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * demande au joueur de rentrer une ligne valide
    * @param tab tableau d'entiers
    * @return numéro de ligne valide rentré par le joueur
    **/
    int demandeLigne(int[] tab) {
        int ret;
        if (!unSeulTasSeparable(tab)) {
            ret = SimpleInput.getInt("Ligne : ");
            while (!bonChoixLigne(tab, ret)) {
                ret = SimpleInput.getInt("Ligne : ");
            }
        } else {
            ret = indiceDuSeulTasSeparable(tab);
        }
        return ret;
    }
    
    /**
    * teste de demandeLigne
    **/
    void testDemandeLigne() {
        System.out.println();
		System.out.println("*** testDemandeLigne()");
        
        System.out.println("rentrez 0 puis 1 puis 2 : ");
        System.out.println("le programme renvoit ok si la méthode marche bien");
        int[] a = {2, 1, 5, 3, 0, 0, 0, 0, 0};
        testCasDemandeLigne(a, 2);
        
        System.out.println();
        
        System.out.println("ici vous n'avez rien a rentrer car il n'y a qu'un seul tas separable");
        System.out.println("le programme renvoit ok si la méthode marche bien");
        int[] b = {2, 1, 5, 2, 0, 0, 0, 0, 0};
        testCasDemandeLigne(b, 2);
    }
   
    /**
    * teste un appel de demandeLigne
    * @param tab tableau d'entiers
    * @param result resultat attendu
    **/
    void testCasDemandeLigne(int[] tab, int result) {
        // Arrange
        System.out.println("demandeLigne (" + Arrays.toString(tab) + ") \t= " + result + "\t ");
        // Act
        int resExec = demandeLigne(tab);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * demande au joueur de rentrer une separation valide
    * @param tab tableau d'entiers
    * @param ligne ligne valide
    * @return une séparation valide rentrée par le joueur
    **/
    int demandeSeparer(int[] tab, int ligne) {
        int ret = SimpleInput.getInt("Nombre d'allumettes à séparer : ");
        while (!bonneSeparation(tab, ligne, ret)) {
            ret = SimpleInput.getInt("Nombre d'allumettes à séparer : ");
        }
        return ret;
    }
    
    /**
    * teste de demandeSeparer
    **/
    void testDemandeSeparer() {
        System.out.println();
		System.out.println("*** testDemandeSeparer()");
        
        System.out.println("rentrez 0 puis 5 puis 3 : ");
        System.out.println("le programme renvoit ok si la méthode marche bien");
        int[] a = {2, 1, 5, 3, 0, 0, 0, 0, 0};
        testCasDemandeSeparer(a, 2, 3);
    }
   
    /**
    * teste un appel de demandeSeparer
    * @param tab tableau d'entiers
    * @param ligne ligne valide
    * @param result resultat attendu
    **/
    void testCasDemandeSeparer(int[] tab, int ligne, int result) {
        // Arrange
        System.out.println("demandeSeparer (" + Arrays.toString(tab) + ", " + ligne + ") \t= " + result + "\t ");
        // Act
        int resExec = demandeSeparer(tab, ligne);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * teste si un tableau ne contient que des nombres inférieurs ou égaux à 2
    * @param tab tableau d'entiers
    * @return vrai ssi un tableau ne contient qeu des nombres inférieurs ou égaux à 2
    **/
    boolean finPartie(int[] tab) {
        boolean ret = false;
        int i = 0;
        boolean sort = true;
        while (i < tab.length) {
            if (tab[i] > 2) {
                sort = false;
            } 
            i = i + 1;
        }
        if (!sort) {
            ret = false;
        } else {
            ret = true;
        }
        return ret;
    }
    
    /**
    * teste de finPartie
    **/
    void testFinPartie() {
        System.out.println();
		System.out.println("*** testFinPartie()");
        
        int[] a = {8, 4, 6, 3};
        testCasFinPartie(a, false);
        int[] b = {2, 1, 2, 1};
        testCasFinPartie(b, true);
        int[] c = {2, 4, 1, 3};
        testCasFinPartie(c, false);
        int[] d = {2};
        testCasFinPartie(d, true);
        int[] e = {1, 1, 1, 3};
        testCasFinPartie(e, false);
        int[] f = {2, 1, 2, 1, 2, 1, 1};
        testCasFinPartie(f, true);
    }
   
    /**
    * teste un appel de finPartie
    * @param tab tableau d'entier
    * @param result resultat attendu
    **/
    void testCasFinPartie(int[] tab, boolean result) {
        // Arrange
        System.out.print("finPartie (" + Arrays.toString(tab) + ") \t= " + result + "\t : ");
        // Act
        boolean resExec = finPartie(tab);
        // Assert
        if (resExec == result){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
    /**
    * affiche une ligne
    **/
    void ligneDeSeparation() {
        System.out.println();
        System.out.println("-----------------");
        System.out.println();
    }
    
    /**
    * teste de ligneDeSeparation
    **/
    void testLigneDeSeparation() {
        System.out.println();
		System.out.println("*** testLigneDeSeparation()");
        
        ligneDeSeparation();
    }
    
    /**
    * joue une partie joueur contre joueur
    **/
    void partieJoueurContreJoueur() {
        // Initialisation du tableau d'allumettes
        int[] tabAllumettes = demandeNombreAllumettes();
       
        // Demande les noms des joueurs
        String joueur1 = SimpleInput.getString("Nom du joueur numéro 1 : ");
        String joueur2 = SimpleInput.getString("Nom du joueur numéro 2 : ");
        ligneDeSeparation();
        
        // Affichage de la ligne 0 contenant un certains nombre d'allumettes
        creerEtAfficherLesTableaux(tabAllumettes);
        
        // Le joueur1 commence à jouer
        int joueur = 0;
        String quiJoue = joueur1;
        
        // La partie continue tant que l'on peut séparer les allumettes
        while (!finPartie(tabAllumettes)) {
            
            // Détermine qui va jouer cette manche et affichage du nom
            quiJoue = quiVaJouer(joueur, joueur1, joueur2);
            ligneDeSeparation();
            System.out.println("Tour du joueur : " + quiJoue);
            
            // Demande la ligne au joueur
            int ligne = demandeLigne(tabAllumettes);
           
            // Demande le nombre d'allumettes qu'il veut séparer
            int separer = demandeSeparer(tabAllumettes, ligne);
            
            // Sépare les allumettes comme l'a indiqué le joueur
            separation(tabAllumettes, ligne, separer);
            ligneDeSeparation();
           
            // On affiche les tableaux d'allumettes
            creerEtAfficherLesTableaux(tabAllumettes);
            
            // On incrémente la variable joueur pour changer de joueur
            joueur = joueur + 1;
        }
        
        // Affichage du nom du gagnant
        ligneDeSeparation();
        System.out.println(quiJoue + " a gagné !");
    }
    
    /**
    * Teste de partieJoueurContreJoueur 
    **/
    void testPartieJoueurContreJoueur() {
        System.out.println();
		System.out.println("*** testPartieJoueurContreJoueur()");
        partieJoueurContreJoueur();
    }
}
